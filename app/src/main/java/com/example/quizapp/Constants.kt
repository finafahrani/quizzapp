package com.example.quizapp

object Constants{

    const val USER_NAME: String ="user_name"
    const val TOTAL_QUESTIONS: String = "total_question"
    const val CORRECT_ANSWER: String = "correct_answers"

    fun getQuestions(): ArrayList<Question>{
        val questionsList = ArrayList<Question>()

        //1
        val que1 = Question(
            1,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_australia),
            "Australia",
            "Austria",
            "New Zaeland",
            "Papua New Guinea",
            1
        )
        questionsList.add(que1)

        //2
        val que2 = Question(
            2,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_india),
            "Nigeria",
            "Hungary",
            "India",
            "Pakistan",
            3
        )
        questionsList.add(que2)

        //3
        val que3 = Question(
            3,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_malaysia),
            "Singapore",
            "Malaysia",
            "United State of America",
            "United Kingdom",
            2
        )
        questionsList.add(que3)

        //4
        val que4 = Question(
            4,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_new_zealand),
            "Australia",
            "Austria",
            "New Zaeland",
            "Papua New Guinea",
            3
        )
        questionsList.add(que4)

        //5
        val que5 = Question(
            5,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_saudi_arabia),
            "Saudi Arabia",
            "United Arab Emirates",
            "Iran",
            "Iraq",
            1
        )
        questionsList.add(que5)

        //6
        val que6 = Question(
            6,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_south_korea),
            "People's Republic of China",
            "Japan",
            "North Korea",
            "South Korea",
            4
        )
        questionsList.add(que6)

        //7
        val que7 = Question(
            7,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_the_united_arab_emirates),
            "Saudi Arabia",
            "United Arab Emirates",
            "Iran",
            "Iraq",
            2
        )
        questionsList.add(que7)


        //8
        val que8 = Question(
            8,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_turkey),
            "Tunisia",
            "Turkey",
            "Pakistan",
            "Algeria",
            2
        )
        questionsList.add(que8)


        //9
        val que9 = Question(
            9,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_vietnam),
            "Brunei Darussalam",
            "Thailand",
            "Philippines",
            "Vietnam",
            4
        )
        questionsList.add(que9)

        //10
        val que10 = Question(
            10,
            "What country does this flag belong to?",
            (R.drawable.ic_flag_of_yemen),
            "Syria",
            "Sudan",
            "Egypt",
            "Yemen",
            4
        )
        questionsList.add(que10)


        return questionsList
    }

}


